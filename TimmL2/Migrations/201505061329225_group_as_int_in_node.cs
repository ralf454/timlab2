namespace TimmL2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class group_as_int_in_node : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Nodes", "Group", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Nodes", "Group", c => c.String());
        }
    }
}
