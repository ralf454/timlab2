namespace TimmL2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Links",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.Int(nullable: false),
                        NodeSourceId = c.Int(nullable: false),
                        NodeTargetId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                
                .ForeignKey("dbo.Nodes", t => t.NodeSourceId, cascadeDelete: true)
                .Index(t => t.NodeSourceId)
                .Index(t => t.NodeTargetId);
            
            CreateTable(
                "dbo.Nodes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Group = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Links", "NodeSourceId", "dbo.Nodes");
            DropIndex("dbo.Links", new[] { "NodeTargetId" });
            DropIndex("dbo.Links", new[] { "NodeSourceId" });
            DropTable("dbo.Nodes");
            DropTable("dbo.Links");
        }
    }
}
