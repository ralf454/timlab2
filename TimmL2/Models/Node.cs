﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml;

namespace TimmL2.Models
{
    public class Node
    {
        public Node()
        {
            NodesSource = new HashSet<Link>();
            NodesTarget = new HashSet<Link>();
        }
        [JsonIgnore]
        public int Id { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }
       
        [JsonProperty("group")]
        public int Group { get; set; }
       [JsonIgnore]
        public virtual HashSet<Link> NodesSource { get; set; }
       [JsonIgnore]
        public virtual HashSet<Link> NodesTarget { get; set; }
       public string createJson()
       {
           var jsonNodes = new WebClient().DownloadString("http://localhost:26857/api/nodes");
           var jsonLinks = new WebClient().DownloadString("http://localhost:26857/api/links");
           string outString="{\"nodes\":" + jsonNodes + ",\"links\":" + jsonLinks + "}";
           return outString;
 
           //using (FileStream fs = File.Open(@"c:\person.json", FileMode.CreateNew))
           //using (StreamWriter sw = new StreamWriter(fs))
           //using (JsonWriter jw = new JsonTextWriter(sw))
           //{
           //    jw.Formatting = Formatting.Indented;

           //    JsonSerializer serializer = new JsonSerializer();
           //    serializer.Serialize(jw, person);
           //}
           //List<Employee> eList = new List<Employee>();
           //Employee e = new Employee();
           //e.Name = "Minal";
           //e.Age = 24;

           //eList.Add(e);

           //e = new Employee();
           //e.Name = "Santosh";
           //e.Age = 24;

           //eList.Add(e);

           //string ans = JsonConvert.SerializeObject(eList, Formatting.Indented);

           //string script = "var employeeList = {\"Employee\": " + ans+"};";
           //script += "for(i = 0;i<employeeList.Employee.length;i++)";
           //script += "{";
           //script += "alert ('Name : ='+employeeList.Employee[i].Name+' 
           //Age : = '+employeeList.Employee[i].Age);";
           //script += "}";

           //ClientScriptManager cs = Page.ClientScript;
           //cs.RegisterStartupScript(Page.GetType(), "JSON", script, true);


           //return 
       }
       public void saveXML()
       {
           var json = new WebClient().DownloadString("http://localhost:26857/api/graph");
           XmlDocument doc = (XmlDocument)Newtonsoft.Json.JsonConvert.DeserializeXmlNode(json);
           XmlTextWriter writer = new XmlTextWriter("json.xml", null);
           writer.Formatting =System.Xml.Formatting.Indented;
           doc.Save(writer);
       }
    }
}
