﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimmL2.Models
{
    public class Link
    {
        public Link() { }
        [JsonIgnore]
        public int Id { get; set; }
        
        [JsonIgnore]
        public int  NodeSourceId { get; set; }
        [JsonIgnore]
        public virtual Node NodeSource { get; set; }
        [JsonIgnore]
        public int NodeTargetId { get; set; }
        
        [JsonProperty("value")]
        public int Value { get; set; }
        [JsonIgnore]
        public virtual Node NodeTarget { get; set; }

        [JsonProperty("target")]
        public string NodeTargetName;
        [JsonProperty("source")]
        public string NodeSourceName;
    }

}
