﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;

namespace TimmL2.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("TIMGraph")
        {
        }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Link>()
             .HasRequired<Node>(s => s.NodeSource)
                      .WithMany(s => s.NodesSource)
                      .HasForeignKey(s => s.NodeSourceId);

            //modelBuilder.Entity<Link>() //nawet pd pokazywał że tylko jeden 
            // .HasRequired<Node>(s => s.NodeTarget)
            //          .WithMany(s => s.NodesTarget)
            //          .HasForeignKey(s => s.NodeTargetId);
        }

        public System.Data.Entity.DbSet<TimmL2.Models.Link> Links { get; set; }

        public System.Data.Entity.DbSet<TimmL2.Models.Node> Nodes { get; set; }
    }
}