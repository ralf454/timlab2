﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using TimmL2.Models;

namespace TimmL2.Controllers
{
    public class Graph3Controller : Controller
    {
        // GET: Graph3
       

        public string Index()
        {
            return "test";
        }
        public string addNode(String name, int group)
        {
            Node node = new Node();
            node.Name = name;
            node.Group = group;
            return new NodesController().PostNode(node).ToString();
        }
        public string removeNode(string name)
        {
            Node findNode= new NodesController().GetNodes().First(a=>a.Name.Equals(name));
            List<Link> findLink=new List<Link>();
            findLink.AddRange(new LinksController().GetLinks().Where(a => a.NodeSourceId == findNode.Id).AsEnumerable());
            findLink.AddRange(new LinksController().GetLinks().Where(a => a.NodeTargetId == findNode.Id).AsEnumerable());
            if (findLink!=null)
            {
                foreach (var item in findLink)
	            {
		         new LinksController().DeleteLink(item.Id).ToString();  
	            }
                
            }
           
            return new NodesController().DeleteNode(findNode.Id).ToString();
        }
       public string  addLink(String NodeSource, String NodeTarget, int Value)
        {
            Node findNodeSource = new NodesController().GetNodes().First(a => a.Name.Equals(NodeSource));
            Node findNodeTarget = new NodesController().GetNodes().First(a => a.Name.Equals(NodeTarget));

           Link  link=new Link()
           {
               NodeSourceId=findNodeSource.Id,
               NodeTargetId=findNodeTarget.Id,
               Value=Value
           };
           return new LinksController().PostLink(link).ToString();  
        }
        public string removeLink(String removeSourceNode, String removeTargetNode)
       {
            Node nodeSource=new NodesController().GetNodes().First(a => a.Name.Equals(removeSourceNode));
            Node nodeTarget=new NodesController().GetNodes().First(a => a.Name.Equals(removeTargetNode));

           IQueryable<Link> listfind = new LinksController().GetLinks().Where(a => a.NodeSourceId == nodeSource.Id).AsQueryable();
           Link linkfind= listfind.First(a => a.NodeTargetId == nodeTarget.Id);

           return new LinksController().DeleteLink(linkfind.Id).ToString();
       }
        public XmlDocument saveXML()
        {
            var json= new WebClient().DownloadString("http://localhost:26857/api/graph");
            XmlDocument doc = (XmlDocument)Newtonsoft.Json.JsonConvert.DeserializeXmlNode(json);
            XmlTextWriter writer = new XmlTextWriter("json.xml", null);
            writer.Formatting = Formatting.Indented;
            doc.Save(writer);
            return doc;
        }
    }
}