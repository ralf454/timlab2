﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using TimmL2.Models;

namespace TimmL2.Controllers
{
    public class GraphController : ApiController
    {
       
        public JObject GetGraph()
        {
           var jsonNodes = new WebClient().DownloadString("http://localhost:26857/api/nodes");
           var jsonLinks = new WebClient().DownloadString("http://localhost:26857/api/links");
           string outString="{\"nodes\":" + jsonNodes + ",\"links\":" + jsonLinks + "}";
           JObject json = JObject.Parse(outString);
            return json;
        }
    
        public string addNode(String name, int group)
        {
            Node node = new Node();
            node.Name = name;
            node.Group = group;
            return new NodesController().PostNode(node).ToString();
        }
    }
}
